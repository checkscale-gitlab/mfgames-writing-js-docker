#IMAGE = registry.gitlab.com/mfgames-writing-js/mfgames-writing-js-docker
IMAGE = dmoonfire/mfgames-writing-js
VERSION = $(shell cat VERSION)
VERSION2 = $(shell echo $(VERSION) | cut -f 1-2 -d .)
VERSION3 = $(shell echo $(VERSION) | cut -f 1 -d .)

build:
	docker build -t $(IMAGE):latest .

push: build
	docker tag $(IMAGE):latest $(IMAGE):$(VERSION)
	docker tag $(IMAGE):latest $(IMAGE):$(VERSION2)
	docker tag $(IMAGE):latest $(IMAGE):$(VERSION3)
	docker push $(IMAGE):latest
	docker push $(IMAGE):$(VERSION)
	docker push $(IMAGE):$(VERSION2)
	docker push $(IMAGE):$(VERSION3)

enter:
	docker run -it -d $(IMAGE) bin/bash
	docker exec -it $$(docker ps -lq) bin/bash || true
	docker stop $$(docker ps -lq)
